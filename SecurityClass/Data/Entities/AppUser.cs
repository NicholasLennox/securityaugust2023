﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecurityClass.Data.Entities
{
    [Table(nameof(AppUser))]
    public class AppUser
    {
        [Key]
        public Guid Id { get; set; }
        public string? Bio { get; set; }
    }
}
