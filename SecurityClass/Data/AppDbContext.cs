﻿using Microsoft.EntityFrameworkCore;
using SecurityClass.Data.Entities;

namespace SecurityClass.Data
{
    public class AppDbContext : DbContext
    {
        public DbSet<AppUser> Users { get; set; }

        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppUser>()
                .HasData(new AppUser()
                {
                    Id = Guid.NewGuid(),
                    Bio = "Jonny long socks"
                });
        }
    }
}
