﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace SecurityClass.Controllers
{
    [Route("api/v1/resources")]
    [ApiController]
    [Authorize]
    public class Resources : ControllerBase
    {
        [HttpGet("public")]
        [AllowAnonymous]
        public ActionResult GetPublic() 
        {
            return Ok(new { Message = "Public resource" });
        }
        
        [HttpGet("protected")]
        public ActionResult GetProtected() 
        {
            return Ok(new { Message = "Protected resource" });
        }
        
        [HttpGet("role")]
        [Authorize(Roles = "ADMIN")]
        public ActionResult GetRole() 
        {
            return Ok(new { Message = "Roles resource" });
        }

        [HttpGet("subject")]
        public ActionResult GetSubject()
        {
            var subject = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            return Ok(new { Subject = subject });
        }
    }
}
